package mapinterface

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	stringclean "raw-checks/string-clean"
)

func ReadCsvHeadersIntoMapBool(filePath string, mapToUse *map[string]bool) {
	fmt.Println("Reading headers into map for: " + filePath)
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read file for header validation " + filePath + " Error: " + err.Error())
	}
	defer file.Close()

	csvReader := csv.NewReader(file)
	topLine, err := csvReader.Read() //read first line
	if err != nil {
		log.Fatal("Unable to parse file as CSV for " + filePath + " Error: " + err.Error())
	}

	for _, col := range topLine {
		CleanWriteToMapStrBool(mapToUse, col, false)
	}
	fmt.Println("Map creation succesful for: " + filePath)
}

func ReadCsvHeadersIntoMapInt(records [][]string, mapToUse *map[string]int) {
	fmt.Println("Reading headers into map...")

	//set row values
	for i, col := range records[0] {
		CleanWriteToMapStrInt(mapToUse, col, i)
	}
	fmt.Println("Map (int) creation succesful")
}

func ReadCSVUUIDsIntoMapInt(records [][]string, mapToUse *map[string]int, uuidColNum int) {
	fmt.Println("Reading UUIDs into map...")

	//set uuid to value of the current record number it is found in
	for recordNum, record := range records {
		if recordNum == 0 {
			continue //skip header
		}
		for colNum, column := range record {
			if colNum == uuidColNum {
				CleanWriteToMapStrInt(mapToUse, column, recordNum)
				break
			}
		}
	}

	fmt.Println("Map (UUID) creation succesful")
}

func InitMapStr(mapToInit *map[string]string) {
	*mapToInit = make(map[string]string)
}

func CleanWriteToMapStrInt(mapToWriteTo *map[string]int, key string, value int) {
	(*mapToWriteTo)[stringclean.Clean(key)] = value
}

func CleanWriteToMapStrStr(mapToWriteTo *map[string]string, key string, value string) {
	(*mapToWriteTo)[stringclean.Clean(key)] = stringclean.Clean(value)
}

func CleanWriteToMapStrBool(mapToWriteTo *map[string]bool, key string, value bool) {
	(*mapToWriteTo)[stringclean.Clean(key)] = value
}

func CleanReadStrStr(mapToRead *map[string]string, key string) string {
	return stringclean.Clean((*mapToRead)[key])
}

func CleanReadStrInt(mapToRead *map[string]int, key string) int {
	return (*mapToRead)[stringclean.Clean(key)]
}
