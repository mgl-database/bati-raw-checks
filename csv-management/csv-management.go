package csvmanagement

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"os"
	stringclean "raw-checks/string-clean"
)

func ReadCSV(filename string) ([][]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func findColumnIndex(headers []string, columnName string) (int, error) {
	for i, header := range headers {
		if stringclean.Clean(header) == columnName {
			return i, nil
		}
	}
	return -1, errors.New("column name not found")
}

func createMapping(records [][]string, pkIndex int) map[string][]string {
	mapping := make(map[string][]string)
	for _, record := range records {
		key := record[pkIndex] // using dynamic primary key index
		mapping[key] = record
	}
	return mapping
}

func mergeCSVs(records1, records2 [][]string, fkIndex int, mapping map[string][]string) ([][]string, error) {
	// Create a slice to hold the merged records
	var mergedRecords [][]string

	// Append headers from both files
	headers1 := records1[0]
	headers2 := records2[0]
	mergedHeaders := append(headers1, headers2...)
	mergedRecords = append(mergedRecords, mergedHeaders) // append mergedHeaders to mergedRecords

	for _, record1 := range records1[1:] { // start from 1 to skip the header
		key := record1[fkIndex]
		if record2, exists := mapping[key]; exists {
			mergedRecord := append(record1, record2...)
			mergedRecords = append(mergedRecords, mergedRecord) // append mergedRecord to mergedRecords
		} else {
			return nil, fmt.Errorf("Could not find a match for the key %v", key)
		}
	}
	return mergedRecords, nil
}

func ReadAndMergeCSV(file1Path, file1FK, file2Path, file2PK string) ([][]string, error) {
	fmt.Println("Merging files: ")
	fmt.Println(file1Path)
	fmt.Println("Foreign key: " + file1FK)
	fmt.Println(file2Path)
	fmt.Println("Primary key: " + file2PK)

	records1, err := ReadCSV(file1Path)
	if err != nil {
		log.Fatal(err)
	}

	records2, err := ReadCSV(file2Path)
	if err != nil {
		log.Fatal(err)
	}

	file1FKIndex, err := findColumnIndex(records1[0], file1FK)
	if err != nil {
		log.Fatal(err)
	}

	file2PKIndex, err := findColumnIndex(records2[0], file2PK)
	if err != nil {
		log.Fatal(err)
	}

	mapping := createMapping(records2, file2PKIndex)

	mergedRecords, err := mergeCSVs(records1, records2, file1FKIndex, mapping)
	if err != nil {
		log.Fatal(err)
	}

	return mergedRecords, nil
}
