package precheck

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	directoryinterface "raw-checks/directory-interface"
	mapinterface "raw-checks/map-interface"

	_ "github.com/mithrandie/csvq-driver"
	"github.com/mithrandie/csvq/lib/query"
)

func checkDuplicates(file string, colName string, inputDir string) {
	fmt.Println("Validating " + colName + " for: " + file)
	duplicateFound := false

	db, err := sql.Open("csvq", inputDir)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := db.Close(); err != nil {
			panic(err)
		}
	}()

	r, err := db.Query("SELECT " + colName + ", count(" + colName + ") FROM `" + file + "` GROUP BY " + colName + " HAVING count(" + colName + ") > 1;")

	if err != nil {
		log.Fatal("Could not check for " + colName + " duplicates in the " + file + ", error: " + err.Error())
	}

	var (
		ec5_uuid string
		count    string
	)

	for r.Next() {
		if err := r.Scan(&ec5_uuid, &count); err != nil {
			if err == sql.ErrNoRows {

			} else if csvqerr, ok := err.(query.Error); ok {
				panic(fmt.Sprintf("Unexpected error: Number: %d  Message: %s", csvqerr.Number(), csvqerr.Message()))
			} else {
				panic("Unexpected error: " + err.Error())
			}
		} else {
			duplicateFound = true
			fmt.Println("Duplicate " + colName + " found in " + file + ": " + ec5_uuid + ". Count: " + count)
		}
	}

	if duplicateFound {
		log.Fatal("Duplicates found in " + file + " - these need to be fixed before continuing. Exiting...")
	}
	fmt.Println("Duplicate " + colName + " validation succesful for: " + file)
}

func checkHeaders(filePath string, headerFilePath string) {
	fmt.Println("Validating headers for: " + filePath + " against: " + headerFilePath)
	rawHeaders := make(map[string]bool)
	//read raw csv headers into map
	mapinterface.ReadCsvHeadersIntoMapBool(headerFilePath, &rawHeaders)
	//fmt.Println(rawHeaders)
	nonMatchFound := false

	//open file
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read file for header validation: " + filePath + " Error: " + err.Error())
	}
	defer file.Close()

	//read file
	csvReader := csv.NewReader(file)
	records, err := csvReader.Read()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for: " + filePath + " Error: " + err.Error())
	}

	//loop through the headers found in the file
	for _, row := range records {
		mapinterface.CleanWriteToMapStrBool(&rawHeaders, row, true)
	}

	for key, val := range rawHeaders {
		if val == false {
			nonMatchFound = true
			fmt.Println("Found non-matching header: " + key + " for: " + filePath + " using reference: " + headerFilePath)
		}
	}

	if nonMatchFound {
		fmt.Println("--------------------------------------------------")
		fmt.Println("---------------Headers do not match---------------")
		fmt.Println("--------------------------------------------------")

	}

	fmt.Println("Header validation finished for: " + filePath)
}

// check the file to see if there are ec5 or DFO_ID duplicates, in addition check if all headers exist
// if any of these critieria fail, just quit
func PreCheck(clinicalFile string, fishingsetFile string, inputDir string, rawClinical string, rawFishingset string, bypassBatiCheck bool) {
	fmt.Println("----------------------------------------------------")
	fmt.Println("-----------Starting pre-check validations-----------")
	fmt.Println("----------------------------------------------------")

	//get full paths of files
	clinicalFilePath := directoryinterface.CombFileAndDir(clinicalFile, inputDir)
	fishingsetFilePath := directoryinterface.CombFileAndDir(fishingsetFile, inputDir)
	//check uuids
	checkDuplicates(clinicalFilePath, "ec5_branch_uuid", inputDir)
	checkDuplicates(fishingsetFilePath, "ec5_uuid", inputDir)
	//check dfo_id and bati_id
	checkDuplicates(clinicalFilePath, "DFO_", inputDir)
	if !bypassBatiCheck {
		checkDuplicates(clinicalFilePath, "BATI_", inputDir)
	}
	//check headers
	checkHeaders(clinicalFilePath, rawClinical)
	checkHeaders(rawClinical, clinicalFilePath)
	//check back and forth so that we know if there are also extra headers in the input
	checkHeaders(fishingsetFilePath, rawFishingset)
	checkHeaders(rawFishingset, fishingsetFilePath)
	fmt.Println("----------------------------------------------------")
	fmt.Println("-----------Pre-check validations complete-----------")
	fmt.Println("----------------------------------------------------")
}
