package main

import (
	"fmt"
	"log"
	"os"
	comparisoncheck "raw-checks/comparison-check"
	directoryinterface "raw-checks/directory-interface"
	precheck "raw-checks/pre-check"
)

func checkDirExists(dir string) bool {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return false
	}
	return true
}

func main() {
	helpFlag := "-h"
	inputDirFlag := "-i"
	outputDirFlag := "-o"
	clinicalFileFlag := "-cl"
	fishingsetFileFlag := "-fi"
	bypassBatiFlag := "-bypassbati"
	bypassBatiVal := false

	args := os.Args
	flagMap := make(map[string]string)
	prevFlag := ""
	getFlagVal := false
	for _, arg := range args[1:] {
		if getFlagVal {
			flagMap[prevFlag] = arg
			getFlagVal = false
			continue
		}
		if arg == "-h" {
			fmt.Println(inputDirFlag + "               -Required- This is the input directory path where your clinical and fishingset files are stored.")
			fmt.Println(outputDirFlag + "               -Required- This is the output directory path where the report will be stored.")
			fmt.Println(clinicalFileFlag + "              -Optional- This is the name of the clinical file ONLY (not path), if it's not specified it will default to: 'branch-1__sample-description.csv'.")
			fmt.Println(fishingsetFileFlag + "              -Optional- This is the name of the fishingset file ONLY (not path), if it's not specified it will default to: 'form-1__bati-fish-health-and-histology-data-sheet.csv'.")
			fmt.Println(bypassBatiFlag + "      -Optional- This skips checks for duplicates, etc. within BATI IDs.'.")
			fmt.Println(helpFlag + "               -Optional- This is the help flag, which will show this dialogue. If this is specified, all other flags will be ignored and program will exit.")
			os.Exit(0)
		}

		if arg == inputDirFlag || arg == outputDirFlag || arg == clinicalFileFlag || arg == fishingsetFileFlag || arg == bypassBatiFlag {
			if arg == bypassBatiFlag {
				bypassBatiVal = true
			} else {
				prevFlag = arg
				getFlagVal = true
			}
		} else {
			fmt.Println("Unknown flag: " + arg + ", ignoring.")
		}
	}

	//get flags
	importDir := flagMap[inputDirFlag]
	outputDir := flagMap[outputDirFlag]
	inputClinicalFile := flagMap[clinicalFileFlag]
	inputFishingsetFile := flagMap[fishingsetFileFlag]
	//dir checks
	if !checkDirExists(importDir) {
		log.Fatal("Import directory: " + importDir + " does not exist. Exiting...")
	}

	if !checkDirExists(outputDir) {
		if err := os.Mkdir(outputDir, 0755); err != nil {
			log.Fatal("Cannot create output dir: " + outputDir + ". Exiting...")
		}
	}
	//bypassBatiVal = true /////////////////////////////////////////////////
	if inputClinicalFile == "" {
		inputClinicalFile = "branch-1__sample-description.csv"
	}

	if inputFishingsetFile == "" {
		inputFishingsetFile = "form-1__bati-fish-health-and-histology-data-sheet.csv"
	}

	if _, err := os.Stat(importDir + "/" + inputClinicalFile); err != nil {
		if _, err := os.Stat(importDir + inputClinicalFile); err != nil {
			log.Fatal("Clinical file not found in import directory, error: " + err.Error())
		}
	}

	if _, err := os.Stat(importDir + "/" + inputFishingsetFile); err != nil {
		if _, err := os.Stat(importDir + inputFishingsetFile); err != nil {
			log.Fatal("Clinical file not found in import directory, error: " + err.Error())
		}
	}

	rawClinical := "raw_data/branch-1__sample-description.csv"
	rawFishingset := "raw_data/form-1__bati-fish-health-and-histology-data-sheet.csv"
	rawGoogleSheets := "raw_data/google-sheets.csv"
	//do pre checks
	precheck.PreCheck(inputClinicalFile, inputFishingsetFile, importDir, rawClinical, rawFishingset, bypassBatiVal)

	//move onto comparison checks
	//check epi
	comparisoncheck.ComparisonCheck(
		outputDir,
		directoryinterface.CombFileAndDir(inputClinicalFile, importDir),
		directoryinterface.CombFileAndDir(inputFishingsetFile, importDir),
		rawClinical,
		rawFishingset,
		"epi-report.csv",
	)

	//check google sheets
	comparisoncheck.ComparisonCheck(
		outputDir,
		directoryinterface.CombFileAndDir(inputClinicalFile, importDir),
		directoryinterface.CombFileAndDir(inputFishingsetFile, importDir),
		rawGoogleSheets,
		"",
		"google-report.csv",
	)

}
