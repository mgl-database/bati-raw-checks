Raw-checking for BATI

This is a program built in Golang to compare datasets against each other.

The directory structure is as follows:

raw-checks.exe – this is the executable
raw_data – this is the directory containing the raw datasets
	- branch-1__sample-description.csv – this is the raw fish datasets
	- form-1__bati-fish-health-and-histology-data-sheet.csv – this is the raw fish sampling set data
	- google-sheets.csv – this is the google sheets data

Processing steps

Below actions to be aware of are bold.
Warn means output to command prompt window.
Output means output to file.
Stop means the program will end.

The processing is as follows:
    1. Check input directory, if file names have not been set look for default names (see flags)
    2. Pre-check validations on input files
        A. Check duplicates
            1. Check the column “DFO_” for duplicates
            2. Check the column “BATI_” for duplicates – this can be disabled using the bati bypass flag (see flags)
            3. Check “ec5_uuid” and “ec5_branch_uuid” for duplicates
            4. Stop processing and tell user if duplicates found in any of the above
        B. Validate headers against Epicollect syle raw
            5. Check input (clinical and fish sampling set) headers against raw 
            6. Check raw  (clinical and fish sampling set)  headers against input
            7. Warn user if headers don’t match
    3. Comparison checks
        1. Merge raw files into one file based on the “ec5_branch_owner_uuid” and “ec5_uuid” 
        C. Compare input against Epicollect raw data
            A. Map headers against each other using a hashmap
            B. Compare values with “DFO_” being the primary key
                A. If ID not found, output to report file
                B. If attempting to match column at it does not exist warn user
        D. Compare input against Google Sheets raw data
            A. Map headers against each other using a hashmap
            B. Compare values with “DFO_” being the primary key
                A. If ID not found, output to report file
                B. If attempting to match column at it does not exist warn user
    B. Output all finding to files (directory needs to be set – see flags):
        A. epi-report.csv
        B. google-report.csv

Customization

All of the processing is based off of the raw files, where each of the headers are used as a “back bone” against any input files.

An important note: due to the way Golang handles null values, the first column is ignored in comparisons. All of the ec5_uuids are ignored anyhow, so just leave these in the first column.

Mandatory columns
“DFO_” - this is required to ID the fish – only needed in clinical file
“BATI_” - this is required unless it’s bypassed – only needed in clinical file
“ec5_uuid” – this is required so that we can merge the files together – only needed in clinical file
“ec5_branch_owner_uuid” - this is required so that we can merge the files together – only needed in fish sample set file

These could be changed, but I haven’t the time to go further with this.

Flags

-i               -Required- This is the input directory path where your clinical and fishingset files are stored. This can be relative (based on cd location) or absolute.
-o               -Required- This is the output directory path where the report will be stored.
This can be relative (based on cd location) or absolute.
-cl              -Optional- This is the name of the clinical file ONLY (not path), if it's not specified it will default to: 'branch-1__sample-description.csv'.
-fi              -Optional- This is the name of the fishingset file ONLY (not path), if it's not specified it will default to: 'form-1__bati-fish-health-and-histology-data-sheet.csv'.
-bypassbati      -Optional- This skips checks for duplicates, etc. within BATI IDs.'.
-h               -Optional- This is the help flag, which will show a list of commands. If this is specified, all other flags will be ignored and program will exit.

Example usage
- Put files in 
- Open cmd
- Go to the directory that raw-checks.exe is in
	- cd C:\users\user\Desktop\raw-checks
- raw-checks.exe -i C:\users\user\Desktop\inputfiles -o C:\\users\user\Desktop\outputfiles -bypassbati
	- This will set the input file to the “inputfiles” directory on the desktop, similar with the output file, and the checking of BATI IDs is bypassed
-  raw-checks.exe -i C:\users\user\Desktop\inputfiles -o C:\\users\user\Desktop\outputfiles -bypassbati -cl diff-name.csv
- Same as before but setting the clinical name













Warnings on raw data
- There are two “title” columns in the raw files, so you will get some false positives on these as one will match and one will wont. Feel free to delete this column.
- In the raw data there were several repeating DFO IDs, these were removed as it wouldn’t make sense to compare against them, they were these IDs:





Examples of warnings
This is warning the user that there are non-unique DFO IDs in the input file and will not continue until they are fixed.



The column “I_am_a_header” was found in the sample-description raw data file that was not found in the input files. These do not stop execution.
Several columns were not found in the raw. These could be fixed by changing the column names to match the raw file column names.


The key cannot be found in one of the input files. This means that it cannot merge them. See the first point of “Comparison checks” in the Processing Steps.


Examples of file outputs
Epicollect output

The identifiers on the left have been found to have the values on the right to be different from raw. In addition any that say “ID not found in raw” are IDs that are not seen in the raw file.

Google Sheets output
The identifiers on the left have columns not found in the raw on the right. In addition, O6854 has non-matching data for the column “created_at” and “title” (see Warnings on raw data for “title” warnings).

