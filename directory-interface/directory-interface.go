package directoryinterface

import (
	"runtime"
	"strings"
)

func CombFileAndDir(file string, dir string) string {
	if dir[len(dir)-1] == '/' || dir[len(dir)-1] == '\\' {
		return strings.ReplaceAll(dir+file, "\\", "/")
	} else {
		if runtime.GOOS == "windows" {
			return strings.ReplaceAll(dir+"\\\\"+file, "\\", "/")
		} else {
			return strings.ReplaceAll(dir+"/"+file, "\\", "/")
		}
	}
}
