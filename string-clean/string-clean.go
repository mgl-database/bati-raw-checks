package stringclean

import (
	"regexp"
	"strings"
)

func Clean(toClean string) string {
	reg, _ := regexp.Compile("\\s+") //compile
	clean := reg.ReplaceAllString(toClean, "")
	clean = strings.ReplaceAll(clean, "\ufeff", "")
	return clean
}
