package comparisoncheck

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	csvmanagement "raw-checks/csv-management"
	directoryinterface "raw-checks/directory-interface"
	mapinterface "raw-checks/map-interface"
	stringclean "raw-checks/string-clean"
)

// this is the map that will use the uuid id as the key and store a cumulative comment string that will be output in the final report
var clinicalCommentMap map[string]string
var fishingsetCommentMap map[string]string

func init() {
	clinicalCommentMap = make(map[string]string)
	fishingsetCommentMap = make(map[string]string)
}

// add a comment to the comment map of your choice
func addComment(pk string, newComment string, mapToUse *map[string]string) {
	(*mapToUse)[pk] = (*mapToUse)[stringclean.Clean(pk)] + "," + newComment
}

func writeCommentsToFile(outputPath string, commentMap *map[string]string) {
	//Write each CSV line starting with UUID, then dfo_id
	//move onto fishingset map
	//start with UUID
	csvFile, err := os.Create(outputPath)
	if err != nil {
		fmt.Println("ERROR: Could not create output file: " + outputPath + ". Error: " + err.Error())
	}
	csvFile.Write([]byte("Identifier\n"))
	for key, comment := range *commentMap {
		csvFile.Write([]byte(key + comment + "\n"))
	}
}

func setComparisonMaps(headerMap *map[string]int, uuidMap *map[string]int, records [][]string, primaryKey string) {
	//set header row as header map
	mapinterface.ReadCsvHeadersIntoMapInt(records, headerMap)

	//get the row numbers for the uuids
	//this is done by passing in the column numbers for the header that is the uuid
	mapinterface.ReadCSVUUIDsIntoMapInt(records, uuidMap, (*headerMap)[primaryKey])
}

func getCSVRecords(filePath string) [][]string {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read file: " + filePath + " Error: " + err.Error())
	}
	defer file.Close()

	csvReader := csv.NewReader(file)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for " + filePath + " Error: " + err.Error())
	}
	return records
}

func compareInputToRaw(inputUUIDMap map[string]int, inputHeaderMap map[string]int, inputRecords [][]string, rawUUIDMap map[string]int, rawHeaderMap map[string]int, rawRecords [][]string, uuidColName string, commentMap *map[string]string) {
	fmt.Println("Starting comparison...")
	notFoundInRawCols := make(map[string]bool)
	//inputRecords := getCSVRecords(inputFilePath)
	//rawRecords := getCSVRecords(rawFilePath)

	//loop through each record in the input uuid map
	for uuid, uuidRowNum := range inputUUIDMap { //loop through the input map for IDs, i.e. go through each ID found
		for colName, colNum := range inputHeaderMap { //loop through each header found
			inputVal := stringclean.Clean(inputRecords[uuidRowNum][colNum]) //get the value for this ID/row at this column
			UUIDRowPos := mapinterface.CleanReadStrInt(&rawUUIDMap, uuid)   //get the row position of the same ID in the raw ID map
			if UUIDRowPos == 0 {                                            //this is the default value, so it means that this ID was not found in the raw file
				addComment(uuid, "ID not found in raw.", commentMap)
				break
			} else { //if it is found in the raw headers
				if colName == "ec5_uuid" || colName == "ec5_branch_owner_uuid" || colName == "ec5_branch_uuid" || colName == "uploaded_at" {
					continue //skip checks on the above cols
				}
				headerPos := mapinterface.CleanReadStrInt(&rawHeaderMap, colName) //get the column position of the current column
				if headerPos == 0 {                                               //THIS NEEDS TO BE CHANGED AS 0 COULD BE A VALID INPUT///////////////////////////////////
					if notFoundInRawCols[colName] == false {
						notFoundInRawCols[colName] = true
						fmt.Println("Column: " + colName + " not found in raw")
					}
					continue
				}
				rawVal := stringclean.Clean(rawRecords[UUIDRowPos][headerPos]) //get the value of the current ID at this column
				if stringclean.Clean(inputVal) != stringclean.Clean(rawVal) {  //compare the input value to the raw value
					addComment(uuid, colName, commentMap) //if they do not match, log it
				}
			}
		}
	}
}

func ComparisonCheck(outputDir string, inputClinicalFilePath string, inputFishingsetFilePath string, rawClinicalFilePath string, rawFishingsetFilePath string, outputFile string) {
	clinicalCommentMap = make(map[string]string)
	clinicalFileName := outputFile
	clinicalOutputPath := directoryinterface.CombFileAndDir(clinicalFileName, outputDir)
	fmt.Println("--------------------------------------------------------------------")
	fmt.Println("-----------------Starting comparison validations--------------------")
	fmt.Println("-----------Output will be saved to: " + outputDir + "---------------")
	fmt.Println("--------------------------------------------------------------------")

	//create maps for input and raw
	mergedHeaderMap := make(map[string]int)
	mergedUUIDRowMap := make(map[string]int)
	mergedRawHeaderMap := make(map[string]int)
	mergerdRawUUIDMap := make(map[string]int)

	var mergedRaw [][]string
	var err error
	if rawFishingsetFilePath != "" {
		mergedRaw, err = csvmanagement.ReadAndMergeCSV(rawClinicalFilePath, "ec5_branch_owner_uuid", rawFishingsetFilePath, "ec5_uuid")
		if err != nil {
			log.Fatal("Could not merge RAW files: ", err)
		}
		setComparisonMaps(&mergedRawHeaderMap, &mergerdRawUUIDMap, mergedRaw, "DFO_")
	} else {
		mergedRaw, err = csvmanagement.ReadCSV(inputClinicalFilePath)
		if err != nil {
			log.Fatal("Could not read CSV: ", err)
		}
		setComparisonMaps(&mergedRawHeaderMap, &mergerdRawUUIDMap, mergedRaw, "DFO_")
	}

	mergedInp, err := csvmanagement.ReadAndMergeCSV(inputClinicalFilePath, "ec5_branch_owner_uuid", inputFishingsetFilePath, "ec5_uuid")
	if err != nil {
		log.Fatal("Could not merge INPUT files: ", err)
	}
	setComparisonMaps(&mergedHeaderMap, &mergedUUIDRowMap, mergedInp, "DFO_")

	compareInputToRaw(mergedUUIDRowMap, mergedHeaderMap, mergedInp, mergerdRawUUIDMap, mergedRawHeaderMap, mergedRaw, "DFO_", &clinicalCommentMap)
	//compare input to raw for clinical and fishingset
	writeCommentsToFile(clinicalOutputPath, &clinicalCommentMap)
	fmt.Println("---------------------------------------------------------")
	fmt.Println("-----------Epi comparison validations complete-----------")
	fmt.Println("---------------------------------------------------------")
}
